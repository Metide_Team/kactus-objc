Pod::Spec.new do |spec|
    spec.name = 'kactus'
    spec.version = '1.0.22'
    spec.authors = { 'Andreatta Massimiliano' => 'massimiliano.andreatta@gmail.com' }
    spec.homepage = 'https://github.com/maxandreatta/kactus'
    spec.summary = 'A framework for iOS'
    spec.license = { :type => 'MIT' }
    spec.requires_arc = true
    spec.source = { :git => 'https://github.com/maxandreatta/kactus.git', :tag => "#{spec.version}" }
    spec.source_files = 'kactus/*.{h,m}'
    spec.dependency 'CommonCrypto'
	spec.framework = 'UIKit', 'QuartzCore', 'Foundation', 'Security', 'ExternalAccessory', 'CoreTelephony', 'CoreMotion', 'AudioToolbox'
    spec.platform = :ios, '7.1'
end