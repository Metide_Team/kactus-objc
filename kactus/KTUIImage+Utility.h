//
//  UIImage+Utility.h
//  Kactus-Framework
//
//  Created by Andreatta Massimiliano on 15/07/14.
//  Copyright (c) 2014 Metide Srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utility)

#define TMP NSTemporaryDirectory()

+ (NSString *)mimeTypeByGuessingFromData:(NSData *)data;
+ (UIImage *) imageWithView:(UIView *)view;
+ (NSString *) contentTypeForImageData:(NSData *)data;
+ (UIImage *) imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *) imageFromColor:(UIColor *)color;
+ (UIImage *) image:(UIImage *)image withMaskColor:(UIColor *)color;
+ (UIImage *) tinteggia: (UIImage *)image rosso:(float)r verde:(float)g blu:(float)b alpha:(float)a;
+ (UIImage*)tinteggia:(UIImage *)image fromColor:(UIColor*)colorReceveid withAlpha:(CGFloat)a;

/**
 *  Colora l'immagine con un determinato colore
 *
 *  @param colorReceveid colore con cui colorare l'immagine
 *  @param a             alpha
 *
 *  @return ritorna immagine colorata
 */
- (UIImage*)tinteggiaWithColor:(UIColor*)colorReceveid andAlpha:(CGFloat)a;

+ (void) cacheImage: (NSString *) ImageURLString;
+ (UIImage *) getCachedImage: (NSString *) ImageURLString;
+ (UIImage *) convertBitmapRGBA24ToUIImage:(unsigned char *) bits withSize: (CGSize) size;

+ (UIImage *)imageNamed:(NSString *)name fromBundles:(NSArray*)arrayOfBundles;

@end
