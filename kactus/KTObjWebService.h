

/*
 *************************
 CLASSE DI ERRORE
 *************************
*/

#import <Foundation/Foundation.h>

@interface KTObjWebService : NSObject

@property (strong, nonatomic) id result;
@property (strong, nonatomic) NSURLSessionDataTask *task;
@property (nonatomic) NSInteger statusCode;

@end
