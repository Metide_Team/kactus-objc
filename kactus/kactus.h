//
//  kactus.h
//  kactus
//
//  Created by Andreatta Massimiliano on 25/02/15.
//  Copyright (c) 2015 Metide Srl. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for kactus.
FOUNDATION_EXPORT double kactusVersionNumber;

//! Project version string for kactus.
FOUNDATION_EXPORT const unsigned char kactusVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <kactus/PublicHeader.h>

#import "KTDefineSystem.h"

#import "KTUtility.h"
#import "KTNSDate+Utility.h"
#import "KTUIColor+Utility.h"
#import "KTNSString+LeftPadding.h"
#import "KTNSString+Utility.h"
#import "KTUIView+Utility.h"
#import "KTUIImage+Utility.h"
#import "KTNSMutableAttributedString+HTMLParsing.h"
#import "KTNSString+IPValidation.h"
#import "KTNetworkUtility.h"
#import "KTLocalizationSystem.h"
#import "KTDatabase.h"
#import "KTUILabelWithPadding.h"
#import "KTObjWebService.h"
#import "KTObjWebServiceError.h"
#import "KTObjWebServiceSuccess.h"
#import "KTKeychain.h"

#import "KTNSError+CFStreamError.h"

#import "KTUILabelTopAligned.h"

#import "KTUIFont+DynamicFont.h"