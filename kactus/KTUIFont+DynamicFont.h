//
//  UIFont+DynamicFont.h
//  HelpdeskAdvanced
//
//  Created by developer on 14/03/16.
//  Copyright © 2016 Metide srl. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface KTObjValuesCategories : NSObject

@property(nonatomic) int valueForExtraSmall;
@property(nonatomic) int valueForSmall;
@property(nonatomic) int valueForMedium;
@property(nonatomic) int valueForExtraLarge;
@property(nonatomic) int valueForExtraExtraLarge;
@property(nonatomic) int valueForExtraExtraExtraLarge;

@end



@interface KTObjPercentsCategories : KTObjValuesCategories
/*
 
 Qui i valori interi rappresentano la percentuale
 Es. valueForExtraSmall = 30   --->   30% (del valore iniziale passato come parametro al metodo 'preferredFontWithName')
 
 */
@end



// ********   ESEMPIO DI APPLICAZIONE DI QUESTA CATEGORY  ********

/*
 - (void)viewDidLoad
 {
    [super viewDidLoad];
 
    [self testChangeFont];
 
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(testChangeFont)
                                                 name: UIContentSizeCategoryDidChangeNotification
                                               object: nil];
 }
 
 -(void) viewDidDisappear:(BOOL)animated
 {
    [super viewDidDisappear: animated];
 
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: UIContentSizeCategoryDidChangeNotification
                                                  object: nil];
 }
 
 -(void) testChangeFont
 {
    NSLog(@"set dello slider");
 
    [_lblProva1 setFont: [UIFont preferredFontWithName: kFONT_ROBOTO_BD  withInitSize: 21]];
    [_lblProva1 invalidateIntrinsicContentSize];
 
    [_lblProva2 setFont: [UIFont preferredFontWithName: kFONT_ROBOTO_RG  withInitSize: 15]];
    [_lblProva2 invalidateIntrinsicContentSize];
 
    [_lblProva3 setFont: [UIFont preferredFontWithName: kFONT_ROBOTO_LT  withInitSize: 30]];
    [_lblProva3 invalidateIntrinsicContentSize];
 }
 */

// ***************************************************************


@interface UIFont (DynamicFont)


/**
 *  Imposta font dinamico, rispetto al valore settato sullo slider in Settings, in percentuali preimpostate
 *
 *  @param strNameFont   nome custom font
 *  @param initSize      valore iniziale 'size' del testo (in relazione al valore centrale dello slider: UIContentSizeCategoryLarge)
 
                              CENTRO 
                             (SLIDER)
    ----------------------------O----------------------------
       |       |       |        |        |       |       |
     extra   small   medium   LARGE    extra   extra   extra
     small                             large   extra   extra
                                               large   extra
                                                       large
    ---------------------------------------------------------
 
    UIContentSizeCategoryExtraSmall
    UIContentSizeCategorySmall
    UIContentSizeCategoryMedium
    UIContentSizeCategoryLarge
    UIContentSizeCategoryExtraLarge
    UIContentSizeCategoryExtraExtraLarge
    UIContentSizeCategoryExtraExtraExtraLarge
 
 *
 *  @return font dinamico
 */
+(UIFont *) preferredFontWithName: (NSString *) strNameFont
                     withInitSize: (double) initSize;




/**
 *  Imposta font dinamico, rispetto al valore settato sullo slider in Settings, in percentuali specificate
 *
 *  @param strNameFont           strNameFont nome custom font
 *  @param initSize              valore iniziale 'size' del testo (in relazione al valore centrale dello slider: UIContentSizeCategoryLarge)
 *  @param objPercentsCategories oggetto che contiene le percentuali da impostate nei vari casi dello slider (IMPOSTARE SEMPRE VALORE ASSOLUTO DELLA PERCENTUALE)
 
                             CENTRO
                            (SLIDER)
   ----------------------------O----------------------------
      |       |       |        |        |       |       |
    extra   small   medium   LARGE    extra   extra   extra
    small                             large   extra   extra
                                              large   extra
                                                      large
   ---------------------------------------------------------
 
   UIContentSizeCategoryExtraSmall
   UIContentSizeCategorySmall
   UIContentSizeCategoryMedium
   UIContentSizeCategoryLarge
   UIContentSizeCategoryExtraLarge
   UIContentSizeCategoryExtraExtraLarge
   UIContentSizeCategoryExtraExtraExtraLarge

 *
 *  @return font dinamico
 */
+(UIFont *) preferredFontWithName: (NSString *) strNameFont
                     withInitSize: (double) initSize
         andObjPercentsCategories: (KTObjPercentsCategories *) objPercentsCategories;



/**
 *  Imposta font dinamico, rispetto al valore settato sullo slider in Settings, in valori specificati
 *
 *  @param strNameFont           strNameFont nome custom font
 *  @param initSize              valore iniziale 'size' del testo (in relazione al valore centrale dello slider: UIContentSizeCategoryLarge)
 *  @param objValuesCategories   oggetto che contiene i valori da impostate nei vari casi dello slider
 
                             CENTRO
                            (SLIDER)
   ----------------------------O----------------------------
      |       |       |        |        |       |       |
    extra   small   medium   LARGE    extra   extra   extra
    small                             large   extra   extra
                                              large   extra
                                                      large
   ---------------------------------------------------------
 
   UIContentSizeCategoryExtraSmall
   UIContentSizeCategorySmall
   UIContentSizeCategoryMedium
   UIContentSizeCategoryLarge
   UIContentSizeCategoryExtraLarge
   UIContentSizeCategoryExtraExtraLarge
   UIContentSizeCategoryExtraExtraExtraLarge
 
 *
 *  @return font dinamico
 */
+(UIFont *) preferredFontWithName: (NSString *) strNameFont
                     withInitSize: (double) initSize
           andObjValuesCategories: (KTObjValuesCategories *) objValuesCategories;

@end


