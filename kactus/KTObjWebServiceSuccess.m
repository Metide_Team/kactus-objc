

/*
 *************************
 CLASSE SUCCESS
 *************************
*/

#import "KTObjWebServiceSuccess.h"

@implementation KTObjWebServiceSuccess

- (id)initWithTask:(NSURLSessionDataTask *)task result:(id)result
{
    if(self = [super init])
    {
        self.task = task;
        self.result = result;
        self.statusCode = 200;
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        if(httpResponse != nil)
        {
            self.statusCode = httpResponse.statusCode;
        }
    }
    return self;
}

@end
