//
//  LocalizationSystem.h
//  Kactus
//
//  Created by Andreatta Massimiliano on 31/07/14.
//  Copyright (c) 2014 Metide Srl. All rights reserved.
//

#import <Foundation/Foundation.h>

#define KTLocalizedString(key, comment) \
[[KTLocalizationSystem sharedLocalSystem] localizedStringForKey:(key) value:(comment)]

#define KTLocalizationSetLanguage(language) \
[[KTLocalizationSystem sharedLocalSystem] setLanguage:(language)]

#define KTLocalizationGetLanguage \
[[KTLocalizationSystem sharedLocalSystem] getLanguage]

#define KTLocalizationReset \
[[KTLocalizationSystem sharedLocalSystem] resetLocalization]

@interface KTLocalizationSystem : NSObject {
	NSString *language;
}

// you really shouldn't care about this functions and use the MACROS
+ (KTLocalizationSystem *)sharedLocalSystem;

//gets the string localized
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;

//sets the language
- (void) setLanguage:(NSString*) language;

//gets the current language
- (NSString*) getLanguage;

//resets this system.
- (void) resetLocalization;

@end