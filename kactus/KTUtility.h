#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "KTUIColor+Utility.h"

@interface KTUtility : NSObject

// VALIDATOR
+ (BOOL)validateEmail:(NSString *)email;
+ (BOOL)validatePassword:(NSString *)password;

// ACTIVITY INDICATOR
+ (void)showActivityIndicatorToView:(UIView*)view withType:(UIActivityIndicatorViewStyle)activityIndicatorViewStyle;
+ (void)hideActivityIndicator:(UIView*)view;
+ (UIImageView*)showCustomIndicator:(UIView*)view;
+ (void)showActivityIndicatorWithBackgroundViewToViewController:(UIViewController*)vc;
+ (void)hideActivityIndicatorWithBackgroundViewFromController:(UIViewController*)vc;

// PARSING
+ (NSString*)dictToJSONString:(NSDictionary*)dict;
+ (NSMutableDictionary*)JSONStringtoDictionary:(NSString*)jsonString;
+ (NSMutableArray*)JSONStringtoArray:(NSString*)jsonString;
+ (id)checkInParsing:(id)value;

+ (NSString*)truncateString:(NSString *)stringToTruncate after:(int)maxRange;

+ (UIView*)addGradientViewToBackView:(UIView*)viewParent withFrame:(CGRect)frameGradientView colors:(NSArray*)arrayColors andGradientLocation:(NSArray*)arrayGradientLocation;

/**
 *  Stampa la lista completa dei font di sistema disponibili
 */
+ (void)getListFontsInSystem;

/**
 *  Recupera una cella dato il tipo
 *
 *  @param nibName  xib name
 *  @param objClass class type
 *
 *  @return cella
 */
+ (id)loadNibNamed:(NSString *)nibName ofClass:(Class)objClass;

/**
 *  Recupera il modello del device, per update modelli vedi link http://stackoverflow.com/questions/11197509/ios-how-to-get-device-make-and-model
 *
 *  @return stringa modello device
 */
+ (NSString*)getDeviceName;

/**
 *  Salva gli NSLog in una cartella Logs in documenti solo se non si è in modalità debugger (in run).
 */
+ (void)saveLogsInDocumentsDirectory;

/**
 *  Returns true if the current process is being debugged (either running under the debugger or has a debugger attached post facto).
 *
 *  @return boolean
 */
+ (BOOL)amIBeingDebugged;

@end
