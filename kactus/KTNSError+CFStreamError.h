
#import <Foundation/NSError.h>
#import <CoreFoundation/CFStream.h>

@interface NSError (CFStreamErrorConversion)
+ (NSError *) errorFromCFStreamError: (CFStreamError) streamError;
@end