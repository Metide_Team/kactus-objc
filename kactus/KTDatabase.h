
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KTDatabase : NSObject

/**
 *  Costruisce la query di SELECT in db
 *
 *  @param tableName tabella da cui recuperare
 *  @param arrayOfColumnsToReturn      array di colonne da ritornare
 *  @param dict      dizionario chiave - valore per eventuali where
 *
 *  @return ritorna la stringa automaticamente generata
 */
+ (NSString*)getSQLStringForSelectInTable:(NSString*)tableName
                     withColumnsToReturn:(NSMutableArray*)arrayOfColumnsToReturn
                              whereClause:(NSMutableDictionary*)dictWhereClause;

/**
 *  Costruisce la query di INSERT in db
 *
 *  @param tableName tabella su cui inserire
 *  @param dict      dizionario chiave - valore per l'inserimento
 *
 *  @return ritorna la stringa automaticamente generata
 */
+ (NSString*)getSQLStringForInsertInTable:(NSString*)tableName withDictionary:(NSMutableDictionary*)dict;

/**
 *  Costruisce la query di UPDATE in db con eventuale clausola WHERE
 *
 *  @param tableName       tabella da aggiornare
 *  @param dictKeyValue    dizionario chiave - valore per l'update
 *  @param dictWhereClause dizionario chiave - valore per clausola where
 *
 *  @return ritorna la stringa automaticamente generata
 */
+ (NSString*)getSQLStringForUpdateInTable:(NSString*)tableName
                             withKeyValue:(NSMutableDictionary*)dictKeyValue
                              whereClause:(NSMutableDictionary*)dictWhereClause;

@end
