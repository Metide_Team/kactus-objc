
#import "KTDatabase.h"

@implementation KTDatabase

+ (NSString*)getSQLStringForSelectInTable:(NSString*)tableName
                      withColumnsToReturn:(NSMutableArray*)arrayOfColumnsToReturn
                              whereClause:(NSMutableDictionary*)dictWhereClause {
    
    int cont = 0;
    NSArray *keys;
    NSArray *values;
    
    NSString *sqlStr = [NSString stringWithFormat:@"SELECT "];
    
    if(arrayOfColumnsToReturn.count > 0) {
        
        // Concateno i campi da ritornare se ci sono, altrimenti se nil significa tutti
        for (NSString *strField in arrayOfColumnsToReturn) {
            
            if (cont != 0) {
                
                sqlStr = [sqlStr stringByAppendingString:@", "];
                
            } else {
                
                sqlStr = [sqlStr stringByAppendingString:strField];
                
            }
            
            cont++;
            
        }
        
    } else {
        
        sqlStr = [sqlStr stringByAppendingString:@"* "];
        
    }
    
    // Concateno la tabella
    sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"FROM %@", tableName]];
    
    // Se presente clausola where
    if (dictWhereClause != nil) {
        
        keys = [dictWhereClause allKeys];
        values = [dictWhereClause allValues];
        
        cont = 0;
        sqlStr = [sqlStr stringByAppendingString:@" WHERE "];
        
        for (NSString *key in keys) {
            
            if (cont != 0) {
                
                sqlStr = [sqlStr stringByAppendingString:@" AND "];
                
            }
            
            id value = [values objectAtIndex:cont];
            
            if ([value isKindOfClass:[NSString class]]) {
                
                sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@ = '%@'", key, value]];
                
            } else {
                
                sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@ = %@", key, value]];
                
            }
            
            cont++;
            
        }
        
    }
    
    sqlStr = [sqlStr stringByAppendingString:@";"];
    
    return sqlStr;
    
}

+ (NSString*)getSQLStringForInsertInTable:(NSString*)tableName
             withDictionary:(NSMutableDictionary*)dict {
    
    NSArray *keys = [dict allKeys];
    NSArray *values = [dict allValues];
    
    NSString *sqlStr = [NSString stringWithFormat:@"INSERT INTO %@ ", tableName];
    
    // Apro parentesi chiavi
    sqlStr = [sqlStr stringByAppendingString:@"("];
    
    int cont = 0;
    
    for (NSString *key in keys) {
        
        if (cont != 0) {
            
            sqlStr = [sqlStr stringByAppendingString:@", "];
            
        }
        sqlStr = [sqlStr stringByAppendingString:key];
        
        cont++;
        
    }
    // Chiudo parentesi chiavi
    sqlStr = [sqlStr stringByAppendingString:@") "];
    
    // Apro parentesi valori
    sqlStr = [sqlStr stringByAppendingString:@"VALUES ("];
    
    cont = 0;
    
    for (id value in values) {
        
        if (cont != 0) {
            
            sqlStr = [sqlStr stringByAppendingString:@", "];
            
        }
        
        if ([value isKindOfClass:[NSString class]]) {
            
            sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"'%@'", value]];
            
        } else if ([value isKindOfClass:[NSNull class]]) {
            
            sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"NULL"]];
            
        } else {
            
            sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@", value]];
            
        }
        
        cont++;
        
    }
    
    sqlStr = [sqlStr stringByAppendingString:@");"];
    
    return sqlStr;
    
}

+ (NSString*)getSQLStringForUpdateInTable:(NSString*)tableName
                             withKeyValue:(NSMutableDictionary*)dictKeyValue
                              whereClause:(NSMutableDictionary*)dictWhereClause {
    
    int cont = 0;
    NSArray *keys = [dictKeyValue allKeys];
    NSArray *values = [dictKeyValue allValues];
    
    NSString *sqlStr = [NSString stringWithFormat:@"UPDATE %@ SET ", tableName];
    
    for (NSString *key in keys) {
        
        if (cont != 0) {
            
            sqlStr = [sqlStr stringByAppendingString:@", "];
            
        }
        
        id value = [values objectAtIndex:cont];
        
        if ([value isKindOfClass:[NSString class]]) {
            
            sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@ = '%@'", key, value]];
            
        } else if ([value isKindOfClass:[NSNull class]]) {
            
            sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@ = NULL", key]];
            
        } else {
            
            sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@ = %@", key, value]];
            
        }
        
        cont++;
        
    }
    
    // Se presente clausola where
    if (dictWhereClause != nil) {
        
        keys = [dictWhereClause allKeys];
        values = [dictWhereClause allValues];
        
        cont = 0;
        sqlStr = [sqlStr stringByAppendingString:@" WHERE "];
        
        for (NSString *key in keys) {
            
            if (cont != 0) {
                
                sqlStr = [sqlStr stringByAppendingString:@" AND "];
                
            }
            
            id value = [values objectAtIndex:cont];
            
            if ([value isKindOfClass:[NSString class]]) {
                
                sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@ = '%@'", key, value]];
                
            } else if ([value isKindOfClass:[NSNull class]]) {
                
                sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@ = NULL", key]];
                
            } else {
                
                sqlStr = [sqlStr stringByAppendingString:[NSString stringWithFormat:@"%@ = %@", key, value]];
                
            }
            
            cont++;
            
        }
        
    }
    
    sqlStr = [sqlStr stringByAppendingString:@";"];
    
    return sqlStr;
    
}

@end
