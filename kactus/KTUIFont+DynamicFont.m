//
//  UIFont+DynamicFont.m
//  HelpdeskAdvanced
//
//  Created by developer on 14/03/16.
//  Copyright © 2016 Metide srl. All rights reserved.
//

#import "KTUIFont+DynamicFont.h"


@implementation KTObjValuesCategories

-(id) init
{
    self = [super init];
    if (self)
    {
        self.valueForExtraSmall = 1;
        self.valueForSmall = 1;
        self.valueForMedium = 1;
        self.valueForExtraLarge = 1;
        self.valueForExtraExtraLarge = 1;
        self.valueForExtraExtraExtraLarge = 1;
    }
    return self;
}

@end


@implementation KTObjPercentsCategories

-(id) init
{
    return [super init];
}

@end


@implementation UIFont (DynamicFont)

+(UIFont *) preferredFontWithName: (NSString *) strNameFont
                     withInitSize: (double) initSize
{
    NSAssert(strNameFont != nil && strNameFont.length > 0, @"strNameFont deve essere != nil e non vuota");
    
    double fontSize = initSize;
    double fontSizePercentTwenty = initSize * 20 / 100;
    double fontSizePercentThirty = initSize * 30 / 100;
    
    /*
     
     IN QUESTO CONTESTO SI CONSIDERANO COME PREIMPOSTATE LE PERCENTUALI 20% E 30% DEL VALORE INIZIALE DI SIZE
     
     */
    
    //da settings
    NSString * contentSize = [UIApplication sharedApplication].preferredContentSizeCategory;
    
    if ([contentSize isEqualToString: UIContentSizeCategoryExtraSmall])
    {
        fontSize = fontSize - fontSizePercentThirty;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategorySmall])
    {
        fontSize = fontSize - fontSizePercentTwenty;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryMedium])
    {
        fontSize = fontSize - fontSizePercentTwenty;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryLarge])
    {
        //rimane lo stesso valore di initSize
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraLarge])
    {
        fontSize = fontSize + fontSizePercentTwenty;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraExtraLarge])
    {
        fontSize = fontSize + fontSizePercentTwenty;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraExtraExtraLarge])
    {
        fontSize = fontSize + fontSizePercentThirty;
    }
    
    return [UIFont fontWithName: strNameFont
                           size: fontSize];
}



+(UIFont *) preferredFontWithName: (NSString *) strNameFont
                     withInitSize: (double) initSize
         andObjPercentsCategories: (KTObjPercentsCategories *) objPercentsCategories
{
    NSAssert(strNameFont != nil && strNameFont.length > 0, @"strNameFont deve essere != nil e non vuota");
    NSAssert(objPercentsCategories != nil, @"objPercentsCategories deve essere != nil");
    
    double fontSize = initSize;
    
    //da settings
    NSString * contentSize = [UIApplication sharedApplication].preferredContentSizeCategory;
    
    //set font
    if ([contentSize isEqualToString: UIContentSizeCategoryExtraSmall])
    {
        fontSize = fontSize - (initSize * objPercentsCategories.valueForExtraSmall / 100);
    }
    else if ([contentSize isEqualToString: UIContentSizeCategorySmall])
    {
        fontSize = fontSize - (initSize * objPercentsCategories.valueForSmall / 100);
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryMedium])
    {
        fontSize = fontSize - (initSize * objPercentsCategories.valueForMedium / 100);
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryLarge])
    {
        //rimane lo stesso valore di initSize
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraLarge])
    {
        fontSize = fontSize + (initSize * objPercentsCategories.valueForExtraLarge / 100);
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraExtraLarge])
    {
        fontSize = fontSize + (initSize * objPercentsCategories.valueForExtraExtraLarge / 100);
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraExtraExtraLarge])
    {
        fontSize = fontSize + (initSize * objPercentsCategories.valueForExtraExtraExtraLarge / 100);
    }
    
    return [UIFont fontWithName: strNameFont
                           size: fontSize];
}


+(UIFont *) preferredFontWithName: (NSString *) strNameFont
                     withInitSize: (double) initSize
           andObjValuesCategories: (KTObjValuesCategories *) objValuesCategories
{
    NSAssert(strNameFont != nil && strNameFont.length > 0, @"strNameFont deve essere != nil e non vuota");
    NSAssert(objValuesCategories != nil, @"objValuesCategories deve essere != nil");
    
    double fontSize = initSize;
    
    //da settings
    NSString * contentSize = [UIApplication sharedApplication].preferredContentSizeCategory;
    
    //set font
    if ([contentSize isEqualToString: UIContentSizeCategoryExtraSmall])
    {
        fontSize = objValuesCategories.valueForExtraSmall;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategorySmall])
    {
        fontSize = objValuesCategories.valueForSmall;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryMedium])
    {
        fontSize = objValuesCategories.valueForMedium;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryLarge])
    {
        //rimane lo stesso valore di initSize
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraLarge])
    {
        fontSize = objValuesCategories.valueForExtraLarge;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraExtraLarge])
    {
        fontSize = objValuesCategories.valueForExtraExtraLarge;
    }
    else if ([contentSize isEqualToString: UIContentSizeCategoryExtraExtraExtraLarge])
    {
        fontSize = objValuesCategories.valueForExtraExtraExtraLarge;
    }
    
    return [UIFont fontWithName: strNameFont
                           size: fontSize];

}


@end
