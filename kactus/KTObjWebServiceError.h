

/*
 *************************
 CLASSE DI ERRORE
 *************************
*/

#import <Foundation/Foundation.h>
#import "KTObjWebService.h"

@interface KTObjWebServiceError : KTObjWebService

@property (strong, nonatomic) NSError *error;
@property (strong, nonatomic) NSException *exception;
@property (strong, nonatomic) NSString *strBodyResponse;
@property (strong, nonatomic) NSString *strMessageError;

- (id)initWithTask:(NSURLSessionDataTask *)task error:(NSError *)error;

@end
