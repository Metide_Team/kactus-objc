

/*
 *************************
 CLASSE DI ERRORE
 *************************
*/

#import "KTObjWebServiceError.h"
#import "KTLocalizationSystem.h"

NSString * const KTNetworkingOperationFailingURLResponseDataErrorKey = @"com.alamofire.serialization.response.error.data";

@implementation KTObjWebServiceError

- (id)initWithTask:(NSURLSessionDataTask *)task error:(NSError *)error
{
    if(self = [super init])
    {
        self.task = task;
        self.error = error;
        self.statusCode = -1; //no connectivity
        
        switch (error.code) {
                
            case -1009:
                self.strMessageError = KTLocalizedString(@"key_ws_no_connection", nil);
                break;
                
            default:
                self.strMessageError = KTLocalizedString(@"key_ws_generic_connection_error", nil);
                break;
                
        }
        
        // Estraggo lo status HTTP
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        if(httpResponse != nil)
        {
            self.statusCode = httpResponse.statusCode;
        }
        
        // Estraggo l'eventuale body della risposta
        NSString *strError = [[NSString alloc] initWithData:(NSData*)error.userInfo[KTNetworkingOperationFailingURLResponseDataErrorKey] encoding: NSUTF8StringEncoding];
        self.strBodyResponse = strError;
        
    }
    return self;
}

@end
