

/*
 *************************
 CLASSE SUCCESS
 *************************
*/

#import <Foundation/Foundation.h>
#import "KTObjWebService.h"

@interface KTObjWebServiceSuccess : KTObjWebService

- (id)initWithTask:(NSURLSessionDataTask *)task result:(id)result;

@end
