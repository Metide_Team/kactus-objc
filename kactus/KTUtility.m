

#import <sys/utsname.h>
#import "KTUtility.h"

#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/sysctl.h>

@implementation KTUtility

+ (BOOL)validateEmail:(NSString *)email {
    
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	BOOL isValid = [emailTest evaluateWithObject:email];
	return isValid;
    
}

+ (BOOL)validatePassword:(NSString *)password {
    // Tra 8 e 10 caratteri, almeno 1 maiuscola, almeno 1 minuscola e almeno un numero
    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
    
    NSCharacterSet *upperCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    if (!(password.length >7 && password.length < 11))
    {
        return false;
    }
    
    if ([password rangeOfCharacterFromSet:lowerCaseChars].location == NSNotFound || [password rangeOfCharacterFromSet:upperCaseChars].location == NSNotFound
        || [password rangeOfCharacterFromSet:numbers].location == NSNotFound) {
        return false;
    }
    else
        return true;
}

+ (NSString*)dictToJSONString:(NSDictionary*)dict {
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];

    if (!jsonData) {
        
        NSLog(@"JSON error: %@", error);
        return  nil;
        
    } else {
        
        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        // NSLog(@"JSON OUTPUT: %@",JSONString);
        return JSONString;
        
    }
    
}

+ (NSMutableDictionary*)JSONStringtoDictionary:(NSString*)jsonString {
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    
    if(jsonString.length != 0) {
        
        NSMutableDictionary *myDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        if(!myDictionary) {
            
            NSLog(@"%@",error);
            return  nil;
            
        } else {
            
            return myDictionary;
            
        }
        
    } else {
        
        NSLog(@"empty string");
        return  nil;
        
    }
    
}

+ (NSMutableArray*)JSONStringtoArray:(NSString*)jsonString {
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    
    if(jsonString.length != 0) {
        
        NSMutableArray *arrayNewsByString = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];

        if(!arrayNewsByString) {
            
            NSLog(@"%@",error);
            return  nil;
            
        } else {
            
            return arrayNewsByString;
            
        }
        
    } else {
        
        NSLog(@"empty string");
        return  nil;
        
    }
    
}

+ (void)showActivityIndicatorToView:(UIView*)view withType:(UIActivityIndicatorViewStyle)activityIndicatorViewStyle {
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:activityIndicatorViewStyle];
    
    activityView.center = view.center;
    
    CGRect viewFrame = view.frame;
    CGRect activityViewFrame = activityView.frame;
    activityViewFrame.origin.x = (viewFrame.size.width / 2) - (activityView.frame.size.width / 2);
    activityViewFrame.origin.y = (viewFrame.size.height / 2) - (activityView.frame.size.height / 2);
    activityView.frame = activityViewFrame;
    
    [activityView startAnimating];
    [view addSubview:activityView];
    
}

+ (void) hideActivityIndicator:(UIView*)view {
    
    UIView *activityView;
    NSEnumerator *subviewsEnum = [view.subviews reverseObjectEnumerator];

    for (UIView *subview in subviewsEnum) {
        
		if ([subview isKindOfClass:[UIActivityIndicatorView class]]) {
            
			activityView = subview;
            break;
            
		}
        
	}

    if(activityView) {
        
        [activityView removeFromSuperview];
        activityView = nil;
        
    }
    
}


+ (void)showActivityIndicatorWithBackgroundViewToViewController:(UIViewController*)vc {
    
    UIView *viewParent = vc.view;
    
    UIView *viewBackgroundAlpha = [[UIView alloc] initWithFrame:viewParent.frame];
    viewBackgroundAlpha.backgroundColor = [UIColor blackColor];
    viewBackgroundAlpha.alpha = 0.3;
    viewBackgroundAlpha.tag = 200;
    
    UIView *viewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 180, 60)];
    viewContainer.backgroundColor = [UIColor colorWithHexString:@"f5f5f5" alpha:1.0f];
    viewContainer.layer.cornerRadius = 8;
    viewContainer.tag = 300;
    
    UIActivityIndicatorView *activityView;
    
    #ifdef TARGET_OS_TV
    
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityView.center = viewContainer.center;
    
    #elif TARGET_OS_IOS
    
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.center = viewContainer.center;
    
    #endif
    
    
    CGRect viewFrame = vc.view.frame;
    CGRect viewContainerFrame = viewContainer.frame;
    viewContainerFrame.origin.x = (viewFrame.size.width / 2) - (viewContainerFrame.size.width / 2);
    viewContainerFrame.origin.y = (viewFrame.size.height / 2) - (viewContainerFrame.size.height / 2);
    viewContainer.frame = viewContainerFrame;
    
    [activityView startAnimating];
    
    [viewParent addSubview:viewBackgroundAlpha];
    
    [viewContainer addSubview:activityView];
    [viewParent addSubview:viewContainer];
    
}

+ (void)hideActivityIndicatorWithBackgroundViewFromController:(UIViewController*)vc {
    
    [[vc.view viewWithTag:200] removeFromSuperview];
    [[vc.view viewWithTag:300] removeFromSuperview];
    
}

+ (UIImageView*)showCustomIndicator:(UIView*)view {
    
    UIImageView* imageLoadingFrame = [[UIImageView alloc] initWithFrame:view.bounds];
    NSMutableArray* arrayTempImage = [NSMutableArray array];
    
    for (int iIndex = 0; iIndex < 8; iIndex++) {
    
        NSString *nameImage = [NSString stringWithFormat:@"%04i", iIndex];
        nameImage = [NSString stringWithFormat:@"loader_%@", nameImage];
        [arrayTempImage addObject:[UIImage imageNamed:nameImage]];
        
    }
    
    imageLoadingFrame.animationDuration = 1;
    imageLoadingFrame.contentMode = UIViewContentModeCenter;
    // imageLoadingFrame.backgroundColor = [UIColor colorWithHexString:@"FFFFFF" alpha:0.5];
    imageLoadingFrame.animationImages = [NSArray arrayWithArray:arrayTempImage];
    return imageLoadingFrame;
    
}

+ (NSString*)truncateString:(NSString *)stringToTruncate after:(int)maxRange {
    
    NSRange stringRange = {0, MIN([stringToTruncate length], maxRange)};
    stringRange = [stringToTruncate rangeOfComposedCharacterSequencesForRange:stringRange];
    NSString *shortString = [stringToTruncate substringWithRange:stringRange];
    
    if([stringToTruncate length] > maxRange)
        shortString = [NSString stringWithFormat:@"%@...", shortString];
    return shortString;
    
}

/**
 *  Inserisce una UIView con il gradiente e la ritorna per poterla salvare e manipolare successivamente
 *
 *  @param viewParent
 *  @param frameGradientView
 *  @param arrayColors
 *  @param arrayGradientLocation
 *
 *  @return ViewGradiente
 */
+ (UIView*)addGradientViewToBackView:(UIView*)viewParent withFrame:(CGRect)frameGradientView colors:(NSArray*)arrayColors andGradientLocation:(NSArray*)arrayGradientLocation {
    
    UIView *viewGradient = [[UIView alloc] initWithFrame:frameGradientView];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.opacity = 1;
    gradient.frame = viewGradient.bounds;
    gradient.colors = arrayColors;
    gradient.locations = arrayGradientLocation;
    viewGradient.userInteractionEnabled = NO;
    [viewGradient.layer insertSublayer:gradient atIndex:0];

    [[viewParent superview] insertSubview:viewGradient belowSubview:viewParent];
    
    return viewGradient;
    
}

+ (id)checkInParsing:(id)value {
    
    if(value == [NSNull null]) {
        
        return value = nil;
        
    }
    
    return value;
    
}

+ (void)getListFontsInSystem
{
    
    NSArray *familyNames = [UIFont familyNames];
    
    for( NSString *familyName in familyNames ){
        
        printf( "Family: %s \n", [familyName UTF8String] );
        NSArray *fontNames = [UIFont fontNamesForFamilyName:familyName];
        
        for( NSString *fontName in fontNames ){
            
            printf( "\tFont: %s \n", [fontName UTF8String] );
            
        }
        
    }
    
}

+ (id)loadNibNamed:(NSString *)nibName ofClass:(Class)objClass
{
 
    if (nibName && objClass) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:nibName
                                                         owner:nil
                                                       options:nil];
        for (id currentObject in objects ){
            if ([currentObject isKindOfClass:objClass])
                return currentObject;
        }
        
    }
    
    return nil;
    
}

+ (NSString*)getDeviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      :@"Simulator",
                              @"x86_64"    :@"Simulator",
                              @"iPod1,1"   :@"iPod Touch",        // (Original)
                              @"iPod2,1"   :@"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   :@"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" :@"iPhone",            // (Original)
                              @"iPhone1,2" :@"iPhone",            // (3G)
                              @"iPhone2,1" :@"iPhone",            // (3GS)
                              @"iPad1,1"   :@"iPad",              // (Original)
                              @"iPad2,1"   :@"iPad 2",            //
                              @"iPad3,1"   :@"iPad",              // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",          // (GSM)
                              @"iPhone3,3" :@"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" :@"iPhone 4S",         //
                              @"iPhone5,1" :@"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",              // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",         // (Original)
                              @"iPhone5,3" :@"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" :@"iPhone 6 Plus",     //
                              @"iPhone7,2" :@"iPhone 6",          //
                              @"iPhone8,1" :@"iPhone 6S",         //
                              @"iPhone8,2" :@"iPhone 6S Plus",    //
                              @"iPhone8,4" :@"iPhone SE",         //
                              @"iPad4,1"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   :@"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   :@"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   :@"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}

+ (void)saveLogsInDocumentsDirectory
{

    if(![self amIBeingDebugged])
    {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        // Creo cartella Logs se non esiste
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"Logs"];
        NSError *error = NULL;
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
        
        // Restituisce data in base al timezone
        NSDate *currentDate = [NSDate date];
        NSInteger seconds = [[NSTimeZone localTimeZone] secondsFromGMTForDate:currentDate];
        NSString *fileName =[NSString stringWithFormat:@"%@.log", [NSDate dateWithTimeInterval:seconds sinceDate:currentDate]];
        NSString *logFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/Logs/%@", fileName]];
        freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
        
    }

}

+ (BOOL)amIBeingDebugged
{
    int                 junk;
    int                 mib[4];
    struct kinfo_proc   info;
    size_t              size;
    
    // Initialize the flags so that, if sysctl fails for some bizarre
    // reason, we get a predictable result.
    
    info.kp_proc.p_flag = 0;
    
    // Initialize mib, which tells sysctl the info we want, in this case
    // we're looking for information about a specific process ID.
    
    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = getpid();
    
    // Call sysctl.
    
    size = sizeof(info);
    junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
    assert(junk == 0);
    
    // We're being debugged if the P_TRACED flag is set.
    
    return ( (info.kp_proc.p_flag & P_TRACED) != 0 );
}

@end
